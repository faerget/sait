var map;
var markers = [];
window.onload = function () {
    map = L.map('map').setView([44.5664, 38.0648], 13);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        id: 'mapbox.streets'
    }).addTo(map);

    $.ajax('http://geo.nordost-team.ru/api/allobject', {
        dataType: 'json', success: function (Objects, t) {
            for (var i = 0; i < Objects.Points.length; i++) {
                console.log(Objects.Points[i]);
                markers[i] = L.marker([Objects.Points[i].Latitude, Objects.Points[i].Longitude]).addTo(map).on('click', function (e) {
                    console.log(e.target.ObjectURL);
                    $('#mtitle').html(e.target.ObjectName);
                    $('#mframe').attr('src', e.target.ObjectURL)
                    $('#mmodal').modal('toggle');
                });
                markers[i].ObjectURL = Objects.Points[i].ObjectURL;
                markers[i].ObjectName = Objects.Points[i].ObjectName;
                //map.setView([44.5664, 38.0648], 13);
            }
        }
    });
    
}
